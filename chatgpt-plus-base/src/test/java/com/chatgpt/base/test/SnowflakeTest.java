package com.chatgpt.base.test;

import com.chatgpt.base.utils.Snowflake;
import org.junit.Test;

import java.util.Random;

/**
 * @author yangjian
 **/
public class SnowflakeTest {
    @Test
    public void test() throws InterruptedException {
        Snowflake snowflake = new Snowflake(0, 1);
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            System.out.println(snowflake.nextId());
            Thread.sleep(random.nextInt(100));
        }
    }
}
