package com.chatgpt.base.enums;

/**
 * BizVo 错误代码
 *
 * @author yangjian
 * @since 2018/7/25
 */
public enum ErrorCode {
  SUCCESS(0, "SUCCESS"),
  FAIL(1, "系统开小差了"),
  PARAM_ERROR(101, "参数错误"),
  NOT_AUTH(401, "Not Authorized");

  private int code;

  private String message;

  ErrorCode(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
