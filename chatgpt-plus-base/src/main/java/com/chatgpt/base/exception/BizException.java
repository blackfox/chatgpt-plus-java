package com.chatgpt.base.exception;

import com.chatgpt.base.enums.ErrorCode;

/**
 * 通用业务异常类
 *
 * @author yangjian
 */
public class BizException extends RuntimeException {

  private int code;

  public BizException(ErrorCode errorCode) {
    super(errorCode.getMessage());
    this.code = errorCode.getCode();
  }

  public BizException(int code, String message) {
    super(message);
    this.code = code;
  }

  public BizException(String message) {
    super(message);
    this.code = ErrorCode.FAIL.getCode();
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }
}
