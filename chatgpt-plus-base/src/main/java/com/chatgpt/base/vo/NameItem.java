package com.chatgpt.base.vo;


/**
 * 简单的键值对 name-value
 *
 * @author yangjian
 *
 */
public class NameItem {

  /** name */
  public final String name;
  /** value */
  public final Object value;

  public NameItem(String name, Object value) {
    this.name = name;
    this.value = value;
  }

  public static NameItem valueOf(String name, Object value) {
    return new NameItem(name, value);
  }

  public String getName() {
    return name;
  }

  public Object getValue() {
    return value;
  }

  @Override
  public String toString() {
    return name + ":" + value;
  }
}
