package com.chatgpt.base.vo;

import com.chatgpt.base.enums.ErrorCode;
import lombok.Getter;

import java.io.Serializable;

/**
 * http 请求返回统一 VO
 *
 * @author yangjian
 */
@Getter
public class BizVo implements Serializable {

  /** 错误代码,成功返回 0, 否则返回其他 */
  private int code;

  /** 返回提示信息 */
  private String message;

  /** 返回数据 */
  private Object data;

  /** 总记录数 */
  private long count = 0L;

  /** 页码 */
  private int page = 0;

  /** 每页记录数 */
  private int pageSize = 0;

  public BizVo() {}

  public BizVo(int code) {
    this.code = code;
  }

  public static BizVo getInstance(ErrorCode code) {
    return new BizVo(code.getCode()).setMessage(code.getMessage());
  }

  public static BizVo success() {
    return new BizVo(ErrorCode.SUCCESS.getCode());
  }

  public static BizVo success(Object data) {
    return new BizVo(ErrorCode.SUCCESS.getCode()).setData(data);
  }

  public static BizVo fail() {
    return new BizVo(ErrorCode.FAIL.getCode());
  }

  public static BizVo fail(String message) {
    return new BizVo(ErrorCode.FAIL.getCode()).setMessage(message);
  }

  public BizVo setCode(int code) {
    this.code = code;
    return this;
  }

  public BizVo setMessage(String message) {
    this.message = message;
    return this;
  }

  public BizVo setData(Object data) {
    this.data = data;
    return this;
  }

  public BizVo setCount(long count) {
    this.count = count;
    return this;
  }

  public BizVo setPage(int page) {
    this.page = page;
    return this;
  }

  public BizVo setPageSize(int pageSize) {
    this.pageSize = pageSize;
    return this;
  }
}
