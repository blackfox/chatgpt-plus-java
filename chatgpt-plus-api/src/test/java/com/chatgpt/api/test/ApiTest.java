package com.chatgpt.api.test;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ApiTest {

    @Test
    public void openAi() throws IOException {
        String apiKey = "sk-";
        String endpoint = "https://gpt.bemore.lol/v1/chat/completions";
        String prompt = "请问你是GPT4吗";

        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(endpoint);
            httpPost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
            httpPost.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + apiKey);

            String requestData = "{\"model\":\"gpt-3.5-turbo\",\"messages\": [{\"role\": \"user\", \"content\": \"" + prompt + "\"}]}";
            httpPost.setEntity(new StringEntity(requestData, ContentType.APPLICATION_JSON));

            HttpResponse response = httpClient.execute(httpPost);

            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();

                try (BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()))) {
                    StringBuilder responseString = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        responseString.append(line);
                    }

                    // 处理API响应，解析JSON等
                    System.out.println("API 响应: " + responseString.toString());
                }
            } else {
                System.err.println("API 请求失败 with status code: " + statusCode);
            }
        }
    }
}
