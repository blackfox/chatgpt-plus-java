package com.chatgpt.api.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserProfileVo {
    private int id;
    private String mobile;
    private String avatar;
    private JSONObject chatConfig;
    private int calls;
    private int imgCalls;
    private long totalTokens;
    private long tokens;
    private long expiredTime;
    private boolean vip;
}


