package com.chatgpt.api.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChatHistoryVo {
    private int id;

    private String chatId;
    private int userId;
    private int roleId;
    private String type;
    private String icon;
    private String content;
    private int tokens;
    private boolean useContext;
    private long createdAt;
    private long updatedAt;
}
