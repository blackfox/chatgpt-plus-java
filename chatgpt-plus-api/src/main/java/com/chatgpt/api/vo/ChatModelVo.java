package com.chatgpt.api.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChatModelVo {
    private int id;
    private String platform;
    private String name;
    private String value;
    private boolean enabled;
    private int sortNum;
    private int weight;
    private long createdAt;
    private long updatedAt;
}
