package com.chatgpt.api.vo;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChatRoleVo {
    private int id;
    private String key;
    private String name;
    private JSONArray context;
    private String helloMsg;
    private String icon;
    private boolean enable;
    private int sortNum;
    private long createdAt;
    private long updatedAt;


}
