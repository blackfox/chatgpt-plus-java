package com.chatgpt.api.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChatItemVo {
    private int id;
    private int userId;
    private String icon;
    private int roleId;
    private String chatId;
    private int modelId;
    private String title;
    private long createdAt;
    private long updatedAt;
}
