package com.chatgpt.api.service.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mybatis Json 对象自动处理
 *
 * @author yangjian
 */
@MappedJdbcTypes(JdbcType.VARCHAR)
@MappedTypes({JSONObject.class})
public class JsonTypeHandler extends BaseTypeHandler<JSONObject> {
  @Override
  public void setNonNullParameter(
      PreparedStatement preparedStatement, int i, JSONObject o, JdbcType jdbcType)
      throws SQLException {
    preparedStatement.setString(i, JSON.toJSONString(o));
  }

  @Override
  public JSONObject getNullableResult(ResultSet resultSet, String s) throws SQLException {
    String t = resultSet.getString(s);
    return JSON.parseObject(t);
  }

  @Override
  public JSONObject getNullableResult(ResultSet resultSet, int i) throws SQLException {
    String t = resultSet.getString(i);
    return JSON.parseObject(t);
  }

  @Override
  public JSONObject getNullableResult(CallableStatement callableStatement, int i)
      throws SQLException {
    String t = callableStatement.getString(i);
    return JSON.parseObject(t);
  }
}
