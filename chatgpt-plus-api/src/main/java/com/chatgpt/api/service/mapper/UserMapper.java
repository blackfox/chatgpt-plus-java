package com.chatgpt.api.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatgpt.api.service.entity.User;

/**
 * @author yangjian
 */
public interface UserMapper extends BaseMapper<User> {}
