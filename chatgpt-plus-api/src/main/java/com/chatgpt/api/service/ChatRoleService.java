package com.chatgpt.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatgpt.api.service.entity.ChatItem;
import com.chatgpt.api.service.entity.ChatRole;

/**
 * @author yangjian
 */
public interface ChatRoleService extends IService<ChatRole> {}
