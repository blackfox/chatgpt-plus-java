package com.chatgpt.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatgpt.api.service.entity.Config;
import com.chatgpt.api.service.entity.User;

/**
 * @author yangjian
 */
public interface ConfigService extends IService<Config> {}
