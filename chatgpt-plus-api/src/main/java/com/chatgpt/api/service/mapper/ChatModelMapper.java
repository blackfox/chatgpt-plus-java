package com.chatgpt.api.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatgpt.api.service.entity.ChatModel;

/**
 * @author yangjian
 */
public interface ChatModelMapper extends BaseMapper<ChatModel> {
}
