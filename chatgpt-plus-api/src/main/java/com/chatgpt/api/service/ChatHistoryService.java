package com.chatgpt.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatgpt.api.service.entity.ChatHistory;

/**
 * @author yangjian
 */
public interface ChatHistoryService extends IService<ChatHistory> {
}
