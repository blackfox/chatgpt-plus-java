package com.chatgpt.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatgpt.api.service.ChatHistoryService;
import com.chatgpt.api.service.entity.ChatHistory;
import com.chatgpt.api.service.mapper.ChatHistoryMapper;
import org.springframework.stereotype.Service;

/**
 * @author yangjian
 */
@Service
public class ChatHistoryServiceImpl extends ServiceImpl<ChatHistoryMapper, ChatHistory> implements ChatHistoryService {


}
