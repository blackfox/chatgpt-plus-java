package com.chatgpt.api.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatgpt.api.service.entity.Config;
import com.chatgpt.api.service.entity.User;

/**
 * @author yangjian
 */
public interface ConfigMapper extends BaseMapper<Config> {}
