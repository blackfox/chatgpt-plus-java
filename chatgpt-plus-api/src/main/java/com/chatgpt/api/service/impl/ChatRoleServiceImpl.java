package com.chatgpt.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatgpt.api.service.ChatRoleService;
import com.chatgpt.api.service.entity.ChatRole;
import com.chatgpt.api.service.mapper.ChatRoleMapper;
import org.springframework.stereotype.Service;

/**
 * @author yangjian
 */
@Service
public class ChatRoleServiceImpl extends ServiceImpl<ChatRoleMapper, ChatRole> implements ChatRoleService {
}
