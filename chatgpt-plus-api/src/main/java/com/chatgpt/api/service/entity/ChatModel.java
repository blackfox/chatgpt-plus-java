package com.chatgpt.api.service.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author yangjian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "chatgpt_chat_models", autoResultMap = true)
public class ChatModel {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String platform;
    private String name;
    private String value;
    private int sortNum;
    private boolean enabled;
    private int weight;
    // 创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createdAt;
    // 更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updatedAt;
}
