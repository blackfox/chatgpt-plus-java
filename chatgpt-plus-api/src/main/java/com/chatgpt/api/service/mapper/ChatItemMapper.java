package com.chatgpt.api.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatgpt.api.service.entity.ChatItem;
import com.chatgpt.api.service.entity.Config;

/**
 * @author yangjian
 */
public interface ChatItemMapper extends BaseMapper<ChatItem> {}
