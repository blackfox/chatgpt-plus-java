package com.chatgpt.api.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatgpt.api.service.entity.ChatItem;
import com.chatgpt.api.service.entity.ChatRole;

/**
 * @author yangjian
 */
public interface ChatRoleMapper extends BaseMapper<ChatRole> {}
