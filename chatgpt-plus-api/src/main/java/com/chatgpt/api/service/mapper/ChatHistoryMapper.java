package com.chatgpt.api.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatgpt.api.service.entity.ChatHistory;

/**
 * @author yangjian
 */
public interface ChatHistoryMapper extends BaseMapper<ChatHistory> {
}
