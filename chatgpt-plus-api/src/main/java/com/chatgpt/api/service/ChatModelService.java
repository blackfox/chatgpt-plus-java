package com.chatgpt.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatgpt.api.service.entity.ChatModel;

/**
 * @author yangjian
 */
public interface ChatModelService extends IService<ChatModel> {
}
