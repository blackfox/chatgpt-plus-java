package com.chatgpt.api.service.entity;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.*;
import com.chatgpt.api.service.handler.JsonArrayTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author yangjian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "chatgpt_chat_roles", autoResultMap = true)
public class ChatRole {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;
    @TableField(value = "marker")
    private String key;
    @TableField(value = "context_json", typeHandler = JsonArrayTypeHandler.class)
    private JSONArray context;
    private String helloMsg;
    private String icon;
    private boolean enable;
    private int sortNum;

    // 创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createdAt;
    // 更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updatedAt;

}
