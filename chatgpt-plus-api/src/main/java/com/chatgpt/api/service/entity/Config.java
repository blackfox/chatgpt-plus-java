package com.chatgpt.api.service.entity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.chatgpt.api.service.handler.JsonArrayTypeHandler;
import com.chatgpt.api.service.handler.JsonTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.util.Date;

/**
 * @author yangjian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "chatgpt_configs", autoResultMap = true)
public class Config {
  @TableId(type = IdType.AUTO)
  private Integer id;

  @TableField(value = "marker")
  private String key;
  @TableField(typeHandler = JsonTypeHandler.class ,value = "config_json")
  private JSONObject value;
}
