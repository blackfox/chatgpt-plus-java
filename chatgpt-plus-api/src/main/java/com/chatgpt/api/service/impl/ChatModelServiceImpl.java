package com.chatgpt.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatgpt.api.service.ChatModelService;
import com.chatgpt.api.service.entity.ChatModel;
import com.chatgpt.api.service.mapper.ChatModelMapper;
import org.springframework.stereotype.Service;

/**
 * @author yangjian
 */
@Service
public class ChatModelServiceImpl extends ServiceImpl<ChatModelMapper, ChatModel> implements ChatModelService {
    
}
