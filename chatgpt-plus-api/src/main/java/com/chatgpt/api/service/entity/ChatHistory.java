package com.chatgpt.api.service.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author yangjian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "chatgpt_chat_history", autoResultMap = true)
public class ChatHistory {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String chatId;
    private int userId;
    private int roleId;
    private String type;
    private String icon;
    private String content;
    private int tokens;
    private boolean useContext;

    // 创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createdAt;
    // 更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updatedAt;

    // TODO: 支持逻辑删除
    private Date deletedAt;
}
