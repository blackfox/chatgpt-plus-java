package com.chatgpt.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatgpt.api.conf.ChatPlusConfig;
import com.chatgpt.api.service.ConfigService;
import com.chatgpt.api.service.UserService;
import com.chatgpt.api.service.entity.Config;
import com.chatgpt.api.service.entity.User;
import com.chatgpt.api.service.mapper.ConfigMapper;
import com.chatgpt.api.service.mapper.UserMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author yangjian
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements ConfigService {


}
