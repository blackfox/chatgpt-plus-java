package com.chatgpt.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatgpt.api.service.entity.User;

/**
 * User service interface
 * @author yangjian
 */
public interface UserService extends IService<User> {

    String genToken(int userId);

    User getUserFromToken(String token);

}
