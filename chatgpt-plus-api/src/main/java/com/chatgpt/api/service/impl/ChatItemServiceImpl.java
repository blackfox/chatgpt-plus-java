package com.chatgpt.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatgpt.api.service.ChatItemService;
import com.chatgpt.api.service.ConfigService;
import com.chatgpt.api.service.entity.ChatItem;
import com.chatgpt.api.service.entity.Config;
import com.chatgpt.api.service.mapper.ChatItemMapper;
import com.chatgpt.api.service.mapper.ConfigMapper;
import org.springframework.stereotype.Service;

/**
 * @author yangjian
 */
@Service
public class ChatItemServiceImpl extends ServiceImpl<ChatItemMapper, ChatItem> implements ChatItemService {


}
