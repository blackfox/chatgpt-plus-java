package com.chatgpt.api.service.entity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.chatgpt.api.service.handler.JsonArrayTypeHandler;
import com.chatgpt.api.service.handler.JsonTypeHandler;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author yangjian
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "chatgpt_users", autoResultMap = true)
public class User {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String username;
    private String password;
    private String avatar; // 头像地址
    private String salt; // 密码盐
    private long totalTokens;
    private long tokens;
    private int calls;
    private int imgCalls;
    private long expiredTime;
    private boolean status;
    private int lastLoginAt; // 最后登录时间
    private boolean vip; // 是否 VIP
    private String lastLoginIp; // 最后登录 IP

    @TableField(typeHandler = JsonTypeHandler.class, value = "chat_config_json")
    private JSONObject chatConfig;

    @TableField(typeHandler = JsonArrayTypeHandler.class, value = "chat_roles_json")
    private JSONArray chatRoles;

    @TableField(typeHandler = JsonArrayTypeHandler.class, value = "chat_models_json")
    private JSONArray chatModels;
    // 创建时间
    @TableField(fill = FieldFill.INSERT)
    private Date createdAt;
    // 更新时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updatedAt;
}
