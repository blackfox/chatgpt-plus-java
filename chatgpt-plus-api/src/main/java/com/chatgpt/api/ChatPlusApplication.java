package com.chatgpt.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yangjian
 */
@SpringBootApplication
@MapperScan({"com.chatgpt.api.service.mapper"})
public class ChatPlusApplication {

  public static void main(String[] args) {
    SpringApplication.run(ChatPlusApplication.class, args);
  }
}
