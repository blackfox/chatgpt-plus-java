package com.chatgpt.api.conf;

import com.chatgpt.api.interceptor.SessionInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

  private final SessionInterceptor sessionInterceptor;

  public WebConfig(SessionInterceptor sessionInterceptor) {
    this.sessionInterceptor = sessionInterceptor;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(sessionInterceptor)
            .addPathPatterns("/**")
            .excludePathPatterns(
                    "/api/user/login",
                    "/api/user/register",
                    "/api/admin/config/get",
                    "/api/admin/login",
                    "/api/chat/history",
                    "/api/chat/detail",
                    "/api/mj/jobs",
                    "/api/sd/jobs",
                    "/api/payment/**",
                    "/api/captcha/**",
                    "/api/captcha/**")
                     // swagger
                     .excludePathPatterns("/swagger-ui.html", "/swagger-resources/**", "/webjars/**", "/v3/**", "/swagger-ui.html/**")
                     // knife4j
                    .excludePathPatterns("/doc.html", "/doc.html/**") ;

  }

}
