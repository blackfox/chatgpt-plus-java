package com.chatgpt.api.conf;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * ChatPlus 配置对象
 *
 * @author yangjian
 */
@Component
@Data
public class ChatPlusConfig {
  @Resource(name = "sessionConfig")
  private Session session;
  @Resource(name = "redisConfig")
  private Redis redis;

  @Component("sessionConfig")
  @Data
  public static class Session {

    @Value("${app.session.expired-time}")
    private long expiredTime;
    @Value("${app.session.secret-key}")
    private String secretKey;
  }
  @Component("proxyConfig")
  @Data
  public static class Proxy {

    @Value("${app.proxy.host}")
    private String host;

    @Value("${app.proxy.port}")
    private int port;

    @Value("${app.proxy.protocol}")
    private String protocol;
  }

  @Component("redisConfig")
  @Data
  public static class Redis {

    @Value("${app.redis.host}")
    private String host;

    @Value("${app.redis.port}")
    private int port;
  }
}
