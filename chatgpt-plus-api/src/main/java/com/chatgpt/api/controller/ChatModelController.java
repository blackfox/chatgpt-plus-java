package com.chatgpt.api.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chatgpt.api.service.ChatModelService;
import com.chatgpt.api.service.entity.ChatModel;
import com.chatgpt.api.vo.ChatModelVo;
import com.chatgpt.base.vo.BizVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/model")
public class ChatModelController extends BaseController {
    private final ChatModelService chatModelService;

    public ChatModelController(ChatModelService chatModelService) {
        this.chatModelService = chatModelService;
    }

    @GetMapping("/list")
    public BizVo list(HttpServletRequest request) {
        boolean enable = getBooleanParam(request, "enable");
        QueryWrapper<ChatModel> queryWrapper = new QueryWrapper<>();
        if (enable) {
            queryWrapper.eq("enabled", true);
        }
        List<ChatModel> models = chatModelService.list(queryWrapper);
        List<ChatModelVo> modelVos = new ArrayList<>();
        for (ChatModel model : models) {
            ChatModelVo vo = new ChatModelVo();
            BeanUtils.copyProperties(model, vo);
            vo.setCreatedAt(model.getCreatedAt().getTime() / 1000);
            vo.setUpdatedAt(model.getUpdatedAt().getTime() / 1000);
            modelVos.add(vo);
        }
        return BizVo.success(modelVos);
    }
}
