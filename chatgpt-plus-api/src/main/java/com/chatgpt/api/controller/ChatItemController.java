package com.chatgpt.api.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chatgpt.api.service.ChatHistoryService;
import com.chatgpt.api.service.ChatItemService;
import com.chatgpt.api.service.ChatRoleService;
import com.chatgpt.api.service.entity.ChatHistory;
import com.chatgpt.api.service.entity.ChatItem;
import com.chatgpt.api.service.entity.ChatRole;
import com.chatgpt.api.vo.ChatHistoryVo;
import com.chatgpt.api.vo.ChatItemVo;
import com.chatgpt.base.vo.BizVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/chat")
@Slf4j
public class ChatItemController extends BaseController {

    private final ChatItemService chatItemService;
    private final ChatRoleService chatRoleService;
    private final ChatHistoryService chatHistoryService;

    public ChatItemController(ChatItemService chatItemService, ChatRoleService chatRoleService, ChatHistoryService chatHistoryService) {
        this.chatItemService = chatItemService;
        this.chatRoleService = chatRoleService;
        this.chatHistoryService = chatHistoryService;
    }

    @GetMapping("/list")
    public BizVo list(HttpServletRequest request) {
        Integer userId = getIntParam(request, "user_id");
        if (userId == 0) {
            return BizVo.fail().setMessage("Invalid args");
        }

        QueryWrapper<ChatItem> itemQueryWrapper = new QueryWrapper<>();
        itemQueryWrapper.eq("user_id", userId);
        List<ChatItem> chatItems = chatItemService.list(itemQueryWrapper);
        List<ChatItemVo> chatItemVos = new ArrayList<>();
        List<Integer> roleIds = chatItems.stream().map(ChatItem::getRoleId).collect(Collectors.toList());
        QueryWrapper<ChatRole> roleQueryWrapper = new QueryWrapper<>();
        roleQueryWrapper.in("id", roleIds);
        List<ChatRole> roles = chatRoleService.list(roleQueryWrapper);
        Map<Integer, ChatRole> roleMap = roles.stream().collect(Collectors.toMap(ChatRole::getId, role -> role));

        for (ChatItem item : chatItems) {
            ChatItemVo vo = new ChatItemVo();
            // 拷贝数据
            BeanUtils.copyProperties(item, vo);
            vo.setCreatedAt(item.getCreatedAt().getTime() / 1000);
            vo.setUpdatedAt(item.getUpdatedAt().getTime() / 1000);
            vo.setIcon(roleMap.get(item.getRoleId()).getIcon());
            chatItemVos.add(vo);
        }

        return BizVo.success(chatItemVos);
    }

    @GetMapping("/history")
    public BizVo history(HttpServletRequest request) {
        String chatId = getStrParam(request, "chat_id");
        QueryWrapper<ChatHistory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("chat_id", chatId);
        List<ChatHistory> list = chatHistoryService.list(queryWrapper);
        List<ChatHistoryVo> vos = new ArrayList<>();
        for (ChatHistory item : list) {
            ChatHistoryVo vo = new ChatHistoryVo();
            BeanUtils.copyProperties(item, vo);
            vo.setCreatedAt(item.getCreatedAt().getTime() / 1000);
            vo.setUpdatedAt(item.getUpdatedAt().getTime() / 1000);
            vos.add(vo);
        }
        return BizVo.success(vos);
    }
}
