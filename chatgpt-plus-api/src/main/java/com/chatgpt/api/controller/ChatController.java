package com.chatgpt.api.controller;

import com.chatgpt.base.vo.BizVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/api/chat")
public class ChatController {

    @PostMapping("/tokens")
    public BizVo token() {
        return BizVo.success(1024);
    }
}
