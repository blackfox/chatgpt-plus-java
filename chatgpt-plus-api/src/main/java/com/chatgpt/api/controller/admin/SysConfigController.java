package com.chatgpt.api.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.chatgpt.api.controller.BaseController;
import com.chatgpt.api.service.ConfigService;
import com.chatgpt.api.service.entity.Config;
import com.chatgpt.base.vo.BizVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
@RequestMapping("/api/admin/config")
public class SysConfigController extends BaseController {

    private final ConfigService configService;

    public SysConfigController(ConfigService configService) {
        this.configService = configService;
    }


    @GetMapping("/get")
    public BizVo getConfig(HttpServletRequest request) {
        String key = request.getParameter("key");
        QueryWrapper<Config> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(new Config().setKey(key));
        Config config = configService.getOne(queryWrapper);
        return BizVo.success(config);
    }
}
