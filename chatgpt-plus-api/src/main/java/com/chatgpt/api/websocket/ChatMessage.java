package com.chatgpt.api.websocket;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChatMessage {

    private String type;
    private String content;

    public ChatMessage(String type, String content) {
        this.type = type;
        this.content = content;
    }

    public byte[] toByte() {
        return JSON.toJSONBytes(this);
    }
}
