package com.chatgpt.api.handler;

import com.chatgpt.api.exception.UnAuthorizedException;
import com.chatgpt.base.enums.ErrorCode;
import com.chatgpt.base.vo.BizVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;


/**
 * global exception catch handler
 *
 * @author yangjian
 */
@ControllerAdvice
@Slf4j
public class AppExceptionHandler {


	@ExceptionHandler(UnAuthorizedException.class)
	public ResponseEntity<BizVo> handleUnAuthException()
	{
		return new ResponseEntity<>(BizVo.getInstance(ErrorCode.NOT_AUTH), HttpStatus.OK);
	}

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<BizVo> handleRuntimeException(Exception e)
	{
		log.error("Runtime error: ", e);
		String message = e.getMessage();
		if (StringUtils.isBlank(message)) {
			message = ErrorCode.FAIL.getMessage();
		}
		return new ResponseEntity<>(BizVo.fail().setMessage(message), HttpStatus.OK);
	}
}
